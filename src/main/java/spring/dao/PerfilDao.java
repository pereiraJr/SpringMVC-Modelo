package spring.dao;

import java.util.List;

import spring.model.Perfil;

public interface PerfilDao {

	public List<Perfil> listarTodosPerfis();

	public void inserirOuAtualizarPerfil(Perfil perfil);

	public Perfil findPerfilById(int id);

	public void removerPerfil(int id);
}
