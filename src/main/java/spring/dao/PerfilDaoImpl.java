package spring.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import spring.model.Perfil;

@Repository
public class PerfilDaoImpl implements PerfilDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Perfil> listarTodosPerfis() {
		Criteria criteria = getSession().createCriteria(Perfil.class);
		return (List<Perfil>) criteria.list();
	}

	@Override
	public void inserirOuAtualizarPerfil(Perfil perfil) {
		getSession().saveOrUpdate(perfil);
	}

	@Override
	public Perfil findPerfilById(int id) {
		Perfil perfil = (Perfil) getSession().get(Perfil.class, id);
		return perfil;
	}

	@Override
	public void removerPerfil(int id) {
		Perfil perfil = (Perfil) getSession().get(Perfil.class, id);
		getSession().delete(perfil);
	}
}