package spring.service;

import java.util.List;

import org.springframework.stereotype.Service;

import spring.model.Perfil;
@Service
public interface PerfilService {
	
	public List<Perfil> listarTodosPerfis();

	public void inserirOuAtualizarPerfil(Perfil perfil);

	public Perfil findPerfilById(int id);

	public void removerPerfil(int id);
}
