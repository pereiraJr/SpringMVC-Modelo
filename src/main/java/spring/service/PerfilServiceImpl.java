package spring.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import spring.dao.PerfilDao;
import spring.model.Perfil;

@Service
@Transactional
public class PerfilServiceImpl implements PerfilService {

	PerfilDao perfilDao;

	@Autowired
	public void setPerfilDao(PerfilDao perfilDao) {
		this.perfilDao = perfilDao;
	}

	@Transactional
	public List<Perfil> listarTodosPerfis() {
		return perfilDao.listarTodosPerfis();
	}

	@Transactional
	public void inserirOuAtualizarPerfil(Perfil perfil) {
		perfilDao.inserirOuAtualizarPerfil(perfil);
	}

	@Transactional
	public Perfil findPerfilById(int id) {
		return perfilDao.findPerfilById(id);
	}

	@Transactional
	public void removerPerfil(int id) {
		perfilDao.removerPerfil(id);
	}
}