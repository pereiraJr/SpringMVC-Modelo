package spring.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import spring.model.Perfil;
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long>{

}
