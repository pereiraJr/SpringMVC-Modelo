package spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import spring.model.Perfil;
import spring.service.PerfilService;

@Controller
@RequestMapping(value = "/perfil")
public class PerfilController {

	@Autowired
	PerfilService perfilService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView model = new ModelAndView("perfil/list");
//		List<Perfil> list = perfilRepository.findAll();
		List<Perfil> list = perfilService.listarTodosPerfis();
		model.addObject("list", list);
		return model;
	}

	@RequestMapping(value = "/alterar/{id}", method = RequestMethod.GET)
	public ModelAndView atualizar(@PathVariable("id") int id) {
		ModelAndView model = new ModelAndView("perfil/form");
		Perfil perfil = perfilService.findPerfilById(id);
		model.addObject("perfilForm", perfil);
		return model;
	}

	@RequestMapping(value = "/excluir/{id}", method = RequestMethod.GET)
	public ModelAndView remover(@PathVariable("id") int id) {
		perfilService.removerPerfil(id);
		return new ModelAndView("redirect:/perfil/list");
	}

	@RequestMapping(value = "/adicionar", method = RequestMethod.GET)
	public ModelAndView adicionar() {
		ModelAndView model = new ModelAndView("perfil/form");
		Perfil perfil = new Perfil();
		model.addObject("perfilForm", perfil);
		return model;
	}

	@RequestMapping(value = "/salvar", method = RequestMethod.POST)
	public ModelAndView salvar(@ModelAttribute("perfilForm") Perfil perfil) {
		perfilService.inserirOuAtualizarPerfil(perfil);
//		perfilRepository.save(perfil);
		return new ModelAndView("redirect:/perfil/list");
	}
}