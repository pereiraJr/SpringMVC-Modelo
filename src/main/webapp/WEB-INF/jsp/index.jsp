<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<link rel="icon" href="image/favicon-32.jpg" sizes="32x32">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Index</title>

<!-- Bootstrap core CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="http://getbootstrap.com/docs/3.3/examples/sticky-footer-navbar/sticky-footer-navbar.css"></script>
</head>
<body>
	<c:import url="common/topo.jsp"></c:import>
		<!-- Início da página -->
	<div class="container">
		<div class="page-header">
			<h1>Bem vindo ao Sistema de Geração de Boletos - SGB</h1>
		</div>
		<p style="text-align: justify;">TESTE.</p>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="text-muted">SGB - UCSal_2017.2</p>
		</div>
	</footer>

	<!-- Core JS -->
	<script src="bootstrap/js/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
