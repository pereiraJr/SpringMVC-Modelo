<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<link rel="icon" href="image/favicon-32.jpg" sizes="32x32">
<head>
<title>Perfil</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="http://getbootstrap.com/docs/3.3/examples/sticky-footer-navbar/sticky-footer-navbar.css"></script>
<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	line-height: 60px;
	background-color: #f5f5f5;
}
</style>
</head>
<body>
	<!-- Fixed navbar -->
	<c:import url="common/topo.jsp"></c:import>
	<div class="container" style="margin-top: 50px;">
		<div class="page-header">
			<h2>Perfil</h2>
		</div>
		<spring:url value="/perfil/salvar" var="salvar" />
		<form:form action="${salvar }" method="POST"
			modelAttribute="cursoForm">
			<form:hidden path="cod" />
			<div class="form-group">
				<label class="col-sm-2">Descrição</label>
				<div class="col-sm-10">
					<form:input path="nome" cssClass="form-control"
						placeholder="Informe a descrição" required="required"
						autofocus="autofocus" />
				</div>
			</div>
			<div class="form-group"></div>
	</div>
	<div class="clearfix"></div>
	<div class="form-group" style="margin-top: 10px;">
		<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-success">Salvar</button>
			<spring:url value="/curso/list" var="list" />
			<a href="${list }" class="btn btn-default">Cancelar</a>
		</div>
	</div>
	</form:form>
	</div>
	<footer class="footer">
		<div class="container">
			<p class="text-muted">SGB - UCSal_2017.2</p>
		</div>
	</footer>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>