<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<link rel="icon" href="image/favicon-32.jpg" sizes="32x32">
<head>
<title>Perfil</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script
	src="http://getbootstrap.com/docs/3.3/examples/sticky-footer-navbar/sticky-footer-navbar.css"></script>
<style>
.footer {
	position: absolute;
	bottom: 0;
	width: 100%;
	height: 60px;
	line-height: 60px;
	background-color: #f5f5f5;
}
</style>
</head>
<body>
	<!-- Fixed navbar -->
	<c:import url="../common/topo.jsp"></c:import>
	<div class="container" style="margin-top: 50px;">
		<div class="page-header">
			<h2>Perfil</h2>
		</div>
		<spring:url value="/perfil/adicionar" var="adicionar" />
		<div class="form-group">
			<a href="${adicionar }" class="btn btn-primary">Adicionar</a>
		</div>
		<div class="clearfix"></div>
		<div class="form-group">
			<c:if test="${!empty list}">
				<table class="table table-condensed" style="margin-top: 5px;">
					<tr>
						<th style="text-align: center" colspan="2">Ações</th>
						<th style="text-align: center">Descrição</th>
					</tr>
					<c:forEach items="${list}" var="perfil">
						<tr>
							<td><spring:url value="/perfil/alterar/${perfil.id}"
									var="alterar" /> <a href="${alterar }"
								class="btn btn-warning btn-xs">Alterar</a></td>
							<td><spring:url value="/perfil/excluir/${perfil.id}"
									var="excluir" /> <a href="${excluir }"
								class="btn btn-danger btn-xs">Excluir</a></td>
							<td class="col-sm-11">${perfil.descricao}</td>
						</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</div>
	<footer class="footer">
		<div class="container">
			<p class="text-muted">SGB - UCSal_2017.2</p>
		</div>
	</footer>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>